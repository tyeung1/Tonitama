struct GameInfo
    cards::Vector{Card}
    white_start_cards::Vector{Card}
    black_start_cards::Vector{Card}
end

function Base.show(io::IO, gameinfo::GameInfo)
    temp = getfield.(gameinfo.cards, :name)
    write(io, @sprintf("Cards: %s, %s, %s, %s, %s\n", temp[1], temp[2], temp[3], temp[4], temp[5]))
end

struct Game
    info::GameInfo
    tree::Tree{GameState}
end

function Game()
    white = [TigerCard, DragonCard]
    black = [CrabCard, ElephantCard]
    waitcard = MonkeyCard
    allcards = push!(append!(white, black), waitcard)
    gameinfo = GameInfo(allcards, white, black)
    Game(gameinfo, Tree(GameState()))
end

function Base.show(io::IO, game::Game)
    show(io, game.info)
    show(io, game.tree.head.val)
end

function generate!(game::Game, depth=5)
    if depth < 1
        error("Depth must be greater than 0")
    end

    q = Queue{Node}()
    tree = game.tree
    head_node = tree.head
    head_moveid = head_node.val.moveid
    enqueue!(q, head_node)

    while !isempty(q)
        node = dequeue!(q)
        gamestate = node.val
        newstates = generate_next(gamestate)
        newchildren = Node{GameState}[]
        foreach(newstates) do x
            newnode = Node(x)
            push!(newchildren, newnode)

            # stopping condition: we don't want to continue if current gamestate is at depth-1 since we already generated its children
            if (gamestate.moveid - head_moveid) < (depth - 1)
                enqueue!(q, newnode)
            end
        end
        set_children!(tree, node, newchildren)
    end
    tree.info.depth = depth
end