@enum Turn WHITE BLACK

struct GameState
    board::Matrix{Int8}
    turn::Turn
    whitecards::Vector{Card}
    blackcards::Vector{Card}
    waitcard::Card
    moveid::UInt16
    score::Int8
end

function GameState()
    GameState(
        get_initial_board(),
        WHITE,
        [TigerCard, DragonCard], [CrabCard, ElephantCard], MonkeyCard,
        0, 0
    )
end

function GameState(whitecards::Vector{Card}, blackcards::Vector{Card}, waitcard::Card)
    GameState(
        get_initial_board(),
        WHITE,
        whitecards, blackcards, waitcard,
        0, 0
    )
end

function get_initial_board()
    return [-1 .* ones(Int8, 1, 2) Int8(-2) -1 .* ones(Int8, 1, 2);
        zeros(Int8, (3, 5));
        ones(Int8, 1, 2) Int8(2) ones(Int8, 1, 2)]
end

function Base.show(io::IO, gamestate::GameState)
    write(io, @sprintf("%s to move\n", string(gamestate.turn)))
    (nrow, ncol) = Base.size(gamestate.board)
    for i in 1:nrow
        for j in 1:ncol
            write(io, @sprintf("%3d", gamestate.board[i, j]))
        end
        write(io, "\n")
    end
end

function Base.hash(gamestate::GameState, u::UInt64)
    UInt64(5)
end

function get_player_cards(gamestate::GameState)
    if gamestate.turn == WHITE
        gamestate.whitecards
    else
        gamestate.blackcards
    end
end

function generate_next(gamestate::GameState)
    if is_game_over(gamestate)
        return GameState[]
    end
    cards = get_player_cards(gamestate)
    (pawnid, masterid) = _get_pawn_master_id(gamestate.turn)

    nextgamestates = GameState[]
    for pieceloc in findall(x -> x == pawnid || x == masterid, gamestate.board)
        for cardidx in eachindex(cards)
            card = cards[cardidx]
            validmoves = is_valid_move.(Ref(gamestate), Ref(pieceloc), card.moveset)
            newgamestates = create_next_gamestate.(Ref(gamestate), Ref(pieceloc), card.moveset[validmoves], Ref(cardidx))
            append!(nextgamestates, newgamestates)
        end
    end
    nextgamestates
end

function is_game_over(gamestate::GameState)
    board = gamestate.board
    # The game ends when either white or black's master make it to the opposing side's starting master position
    # or when any one of a player's pieces captures the opponent's master
    if board[1, 3] == 2 || board[5, 3] == -2
        return true
    elseif !(2 in board) || !(-2 in board)
        return true
    else
        return false
    end
end

function is_valid_move(gamestate::GameState, pieceloc, move::Tuple)
    board = gamestate.board
    turn = gamestate.turn
    # If it's black's turn, then the relative move is inverted 
    relmove = _get_relative_move(turn, move)
    (pawnid, masterid) = _get_pawn_master_id(gamestate.turn)

    newrow = pieceloc[1] + relmove[1]
    newcol = pieceloc[2] + relmove[2]

    if !checkbounds(Bool, board, newrow, newcol)
        return false
    end

    curr_piece_at_new_loc = board[newrow, newcol]
    if curr_piece_at_new_loc == pawnid || curr_piece_at_new_loc == masterid
        return false
    end

    return true
end

function create_next_gamestate(gamestate::GameState, pieceloc, move::Tuple, cardidx)
    board = gamestate.board
    turn = gamestate.turn
    relmove = _get_relative_move(turn, move)
    newrow = pieceloc[1] + relmove[1]
    newcol = pieceloc[2] + relmove[2]

    newboard = copy(board)
    newboard[newrow, newcol] = board[pieceloc]
    newboard[pieceloc] = 0

    newwhitecards = copy(gamestate.whitecards)
    newblackcards = copy(gamestate.blackcards)
    if turn == WHITE
        newwaitcard = gamestate.whitecards[cardidx]
        newwhitecards[cardidx] = gamestate.waitcard
        newturn = BLACK
    else
        newwaitcard = gamestate.blackcards[cardidx]
        newblackcards[cardidx] = gamestate.waitcard
        newturn = WHITE
    end

    GameState(newboard, newturn, newwhitecards, newblackcards, newwaitcard, gamestate.moveid + 1, 0)
end

function _get_relative_move(turn::Turn, move::Tuple)
    if turn == WHITE
        relmove = move
    else
        relmove = -1 .* move
    end
end

function _get_pawn_master_id(turn::Turn)
    if turn == WHITE
        return (1, 2)
    else
        return (-1, -2)
    end
end