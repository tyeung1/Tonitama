mutable struct Node{T}
    val::T
    children::Vector{Node{T}}
end

function Node(val::T) where {T}
    return Node{T}(val, [])
end

function set_children!(node::Node{T}, child::Node{T}) where {T}
    node.children = [child]
end

function set_children!(node::Node{T}, child::T) where {T}
    node.children = [Node(child)]
end

function set_children!(node::Node{T}, children::Vector{Node{T}}) where {T}
    node.children = children
end

function set_children!(node::Node{T}, children::Vector{T}) where {T}
    node.children = Node{T}.(children)
end

get_children(node::Node) = node.children
