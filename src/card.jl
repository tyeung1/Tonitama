struct Card
    name::String
    moveset::Vector{Tuple}
end

function Base.show(io::IO, card::Card)
    movealias = Dict(0 => "-", -1 => "X", 1 => "O")

    board = zeros(Int8, 5, 5)
    board[3, 3] = -1

    for move in card.moveset
        board[move[1]+3, move[2]+3] = 1
    end

    write(io, @sprintf("%s\n", card.name))
    for i in 1:5
        for j in 1:5
            write(io, @sprintf("%3s", movealias[board[i, j]]))
        end
        write(io, "\n")
    end

end

const NullCard = Card("Null", [])

## Card Move Definitions:
# Vector of Tuples where each Tuple is a move
# 1st element of tuple is row delta and 2nd element is col diff
# Note: 1st row refers to the "top" of the board so 1st element will be inverted (+ -> -, - -> +)
const TigerCard = Card("Tiger", [(-2, 0), (1, 0)])

const DragonCard = Card("Dragon", [(-1, -2), (1, -1), (1, 1), (-1, 2)])

const CrabCard = Card("Crab", [(0, -2), (-1, 0), (0, 2)])

const ElephantCard = Card("Elephant", [(-1, -1), (0, -1), (0, 1), (-1, 1)])

const MonkeyCard = Card("Monkey", [(-1, -1), (1, -1), (1, 1), (-1, 1)])

const MantisCard = Card("Mantis", [(-1, -1), (1, 0), (-1, 1)])

const CraneCard = Card("Crane", [(1, -1), (-1, 0), (1, 1)])

const BoarCard = Card("Boar", [(0, -1), (-1, 0), (0, 1)])

# The following sets of two cards are pairs (moveset is inverted across y-axis)
const FrogCard = Card("Frog", [(0, -2), (-1, -1), (1, 1)])
const RabbitCard = Card("Rabbit", [(0, 2), (-1, 1), (1, -1)])

const GooseCard = Card("Goose", [(-1, -1), (0, -1), (1, 1), (0, 1)])
const RoosterCard = Card("Rooster", [(-1, 1), (0, 1), (1, -1), (0, -1)])

const HorseCard = Card("Horse", [(0, -1), (1, 0), (-1, 0)])
const OxCard = Card("Ox", [(0, 1), (1, 0), (-1, 0)])

const EelCard = Card("Eel", [(-1, -1), (1, -1), (0, 1)])
const CobraCard = Card("Cobra", [(-1, 1), (1, 1), (0, -1)])
