module Tonitama

using Revise, DataStructures, Printf

export Node,
    set_children!,
    get_children

export Tree, TreeInfo, fill!

export GameState, WHITE, BLACK

export Card, TigerCard, DragonCard, CrabCard, ElephantCard, MonkeyCard, MantisCard,
    CraneCard, BoarCard, FrogCard, RabbitCard, GooseCard, RoosterCard, HorseCard, OxCard, EelCard, CobraCard

export Game, GameInfo, generate!

include("node.jl")
include("tree.jl")
include("card.jl")
include("gamestate.jl")
include("game.jl")

end # module Tonitama
