mutable struct TreeInfo
    depth::Any
    size::Any
end

struct Tree{T}
    head::Node{T}
    info::TreeInfo
end

function Tree(head::Node{T}) where {T}
    Tree{T}(head, TreeInfo(1, 1))
end

function Tree(val::T) where {T}
    Tree{T}(Node(val), TreeInfo(0, 1))
end

function set_children!(tree::Tree{T}, node::Node{T}, children::Vector{Node{T}}) where {T}
    set_children!(node, children)
    tree.info.size += length(children)
end

get_depth(tree::Tree) = tree.info.depth
size(tree::Tree) = tree.info.size
